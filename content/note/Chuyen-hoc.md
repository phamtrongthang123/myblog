---
title: "Vật vã thời gian đầu Đại Học"
author: "Pham Trong Thang"
date: 2018-03-18
categories:
  - Experiences
tags:
  - VI
---
Tôi vốn rất thích học, nhất là khi trong môi trường được kích thích.
Hiện tại đã gần giữa học kì II rồi, nhìn lại tôi thấy mình tham quá.
Tôi đã học một lượng kiến thức mà tôi biết tôi có thể quên trong tương lai, và gần hết tất cả, đối với thời gian sau này, tôi cảm thấy không quan trọng.
Học nhiều là tốt nhưng nếu học một cách lạc lối thì lãng phí thời gian.

Tôi đang cố gắng sắp xếp lại toàn bộ thời gian, tôi cố gắng sắp xếp các deadline tôi có thể nhận sớm hơn 1 tuần.
Với hy vọng tôi sẽ quay lại con đường ban đầu mà vẫn kịp tiến độ.
Sự đi lệch đã khiến tôi trả giá.
Học kì II này mọi thứ đều gấp rút hơn so với sức tưởng tượng của tôi, hậu quả cũng không phải khó tưởng tượng: Bể kế hoạch.

Một tuần là quá ngắn ngủi nếu để nắm chắc thứ gì đó. 
Tuy nhiên tôi cũng không được phép vội, cho nên tôi lựa chọn những bước đi tiếp ngắn, có thể so sánh với cách làm việc của Linus Torvalds: "Tôi không nhìn lên trời, chỉ vào những vì sao mà nói một ngày sẽ đến đó, tôi luôn nhìn dưới đất xem có cái hố nào để vá trước khi tôi ngã vào chúng". 

Dù mọi thứ có vẻ sẽ tốt đẹp, tôi cũng không chủ quan vì mental energy tối đa của tôi cũng giảm sút khá nhiều, tôi đoán là do mình sống thiếu thể dục một thời gian dài.
Vậy nên Burn out có thể tới bất cứ lúc nào, tối qua tôi chỉ mới tập trung học được 3 tiếng là tôi bắt đầu mất cảm giác, dù đã áp dụng pomodoro.

Google Calender là bạn đồng hành đắc lực của tôi. Tôi thích kiểm soát trên máy tính, nhưng từ giờ tôi phải học kiểm soát ngày mai.
