---
title: About
author: Trong-Thang Pham
date: '2018-02-08'
---

# Author
![](/post/about_files/profile_img.png)

I'm Trong-Thang Pham, a student of University of Science, Ho Chi Minh City, Vietnam. 

# About this blog

I am having struggled with my forgetting knowledge easily. I don’t know why but a lot of times I forget what I have learned even it was a weak before. Currently, I’m using paper and pen for noting stuff. It’s good enough for the short term, but I lose my notes all the time. Therefore I need a more persistent method like writing a blog.

My goal when writing a post will be easy to read, and the more example the better. Warning alert, English is not my mother language so probably many grammar mistake will be here. It's not because of any special reason that I chose English, I chose it because writing Vietnamese on every Linux-distro is painful and painful and painful and very painful. Anyway, I plan to write in Vietnamese if I have chance, like when using Windows, so there will be 2 languages. I also have tags on them, VI for Vietnamese and EN for English.